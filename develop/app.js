// libreria de api
const unirest = require("unirest");
let req = new unirest(
  "GET",
  "https://mashape-community-urban-dictionary.p.rapidapi.com/define"
);

//DOM
const salidaL = document.getElementById("lista");
const salidaD = document.getElementById("defin");
const entrada = document.querySelector("input");
const botonB = document.querySelector("button");
const botonBorrar = document.querySelector("#cls");

//variables
const busqueda = [];
let diccionario = [];

//app
let app = () => {
  load();
  entrada.addEventListener("keydown", reconocerTecla);
};

// eventos
botonB.addEventListener("click", () => {
  // Para que el formulario no se reinicie
  event.preventDefault;
  buscar();
});

botonBorrar.addEventListener("click", (event) => {
  event.preventDefault;
  localStorage.clear();
  location.reload();
});

//Funciones
function reconocerTecla(e) {
  switch (e.code) {
    case "Backspace":
      borrarTecla();
      mostrarBusqueda();
      break;
    case "Enter":
      buscar();
      break;
    case "KeyQ":
    case "KeyW":
    case "KeyE":
    case "KeyR":
    case "KeyT":
    case "KeyY":
    case "KeyU":
    case "KeyI":
    case "KeyO":
    case "KeyP":
    case "KeyL":
    case "KeyK":
    case "KeyJ":
    case "KeyH":
    case "KeyG":
    case "KeyF":
    case "KeyD":
    case "KeyS":
    case "KeyA":
    case "KeyZ":
    case "KeyX":
    case "KeyC":
    case "KeyV":
    case "KeyB":
    case "KeyN":
    case "KeyM":
      Arrkey = [...e.code];
      guardarTecla(Arrkey[3]);
      mostrarBusqueda();
      break;
    case "Semicolon":
      guardarTecla("ñ");
      mostrarBusqueda();
      break;
    default:
      entrada.value = busqueda.join("");
      break;
  }
}

let borrarTecla = () => {
  if (busqueda.length > 0) {
    busqueda.pop();
  }
};

let guardarTecla = (key) => {
  busqueda.push(key.toLowerCase());
};

let mostrarBusqueda = () => {
  salidaL.textContent = "";
  var expr = new RegExp("^" + busqueda.join(""), "i");
  for (let i = 0; i < diccionario.length; i -= -1) {
    if (expr.test(diccionario[i].nombre)) {
      const newPalabra = document.createElement("p");
      newPalabra.innerText = diccionario[i].nombre;
      salidaL.appendChild(newPalabra);
      newPalabra.addEventListener("dblclick", () => {
        salidaD.textContent = "";
        const newdef = document.createElement("p");
        newdef.innerText = diccionario[i].definicion;
        salidaD.appendChild(newdef);
        newdef.addEventListener("dblclick", () => {
          salidaD.textContent = "";
        })
      });
    }
  }
};

function buscar() {
  req.query({
    term: `${busqueda.join("")}`,
  });

  req.headers({
    "x-rapidapi-host": "mashape-community-urban-dictionary.p.rapidapi.com",
    "x-rapidapi-key": "1b5cb8763dmshe33d1b176d41f17p13b23fjsn6bcf3e3b0234",
    useQueryString: true,
  });

  req.end(function (res) {
    if (res.error) {
      console.log("error");
    }
    guardarPalabra(res);
  });
  salidaL.textContent = "proccessing...";
}

let guardarPalabra = (res) => {
  obtenerPalabras();
  diccionario.push({
    nombre: res.body.list[0].word,
    definicion: res.body.list[0].definition,
  });
  localStorage.setItem("palabras", JSON.stringify(diccionario));
  location.reload();
};

function obtenerPalabras() {
  if (localStorage.getItem("palabras") === null) {
    diccionario = [];
  } else {
    diccionario = JSON.parse(localStorage.getItem("palabras"));
  }
}

let load = () => {
  entrada.focus();
  if (localStorage.getItem("palabras") === null) {
    const errr = document.createElement("p");
    errr.innerText =
    "No has guardado ninguna palabra, busca una y agregala a tu diccionario";
    salidaL.appendChild(errr);
  } else {
    obtenerPalabras();
    for (let i = 0; i < diccionario.length; i -= -1) {
      const newPalabra = document.createElement("p");
      newPalabra.innerText = diccionario[i].nombre;
      salidaL.appendChild(newPalabra);
      newPalabra.addEventListener("dblclick", () => {
        salidaD.textContent = "";
        const newdef = document.createElement("p");
        newdef.innerText = diccionario[i].definicion;
        salidaD.appendChild(newdef);
        newdef.addEventListener("dblclick", () => {
          salidaD.textContent = "";
        })
      });
    }
  }
};

// ejecuto aplicacion madre
app();
