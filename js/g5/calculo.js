//Registrar evento click del ratón al presionar botones de envío
function iniciar()
{
    var btnarea = document.getElementById("area");
    var btnperim = document.getElementById("perimetro");
    if (btnarea.addEventListener)
    {
        btnarea.addEventListener("click", calculararea, false);
    } else
    {
        btnarea.attachEvent("onclick", calculararea);
    }
    if (btnperim.addEventListener)
    {
        btnperim.addEventListener("click", calcularperimetro, false);
    } else
    {
        btnperim.attachEvent("onclick", calcularperimetro);
    }
}

    function calculararea()
    {
        var rect = new rectangulo(
            document.frmrectangulo.txtbase.value,
            document.frmrectangulo.txtaltura.value
        );
        rect.mostrar(rect.calcArea(), " área");
        return false;
    }
    function calcularperimetro()
    {
        var peri = new rectangulo(document.frmrectangulo.txtbase.value,document.frmrectangulo.txtaltura.value);
        peri.mostrar(peri.calcPeri(), "perímetro");
        return false;
    }
    //Creando una clase rectángulo
    class rectangulo {
        constructor(base,altura) {
            this.altura = parseFloat(altura);
            this.base = parseFloat(base);
        }
        get carea() {
            return this.calcArea();
        }
        calcArea () {
            return this.base * this.altura;
        }
        get peri() {
            return this.calcPeri();
        }
        calcPeri () {
            return 2*this.base + 2*this.altura;
        }
        mostrar (valor,tipoc) {
            alert('El ' + tipoc + 'es: ' + valor);
        }
    }
//Asociando función que manejará el evento load al cargar la página
if (window.addEventListener)
{
    window.addEventListener("load", iniciar, false);
} else if (window.attachEvent)
{
    window.attachEvent("onload", iniciar);
}
