// DOM
const nomi = document.getElementById("nombre")
const apei = document.getElementById("apellidos")
const correoi = document.getElementById("email")
const contrai = document.getElementById("contrasena")
const contraci = document.getElementById("contrasenaC")
const fechanaci = document.getElementById("fechanac")
const ingresarbtn = document.getElementById("btn")
const frm = document.getElementById("form")
const salida = document.getElementById("Tabla")
const ti = document.getElementById("ti")
const tn = document.getElementById("tn")
const tcn = document.getElementById("tcn")
const tco = document.getElementById("tco")
const tf = document.getElementById("tf")

class miembro
{
    constructor(nom, ape, contra, correo, fechanac)
    {
        this.nom = nom;
        this.ape = ape;
        this.contra = contra;
        this.correo = correo;
        this.fechanac = fechanac;
        var arr = []
        var ape2
        arr = ape.split('')
        for (let i = 0; i < arr.length; i++)
        {
            if (arr[i] === " ")
            {
                ape2 = arr[i + 1];
                break
            }
        }
        if (!ape2) { ape2 = ape[0] }
        let ran1 = Math.round(Math.random() * 10)
        let ran2 = Math.round(Math.random() * 10)
        let ran3 = Math.round(Math.random() * 10)
        let ran4 = Math.round(Math.random() * 10)
        let anio = new Date();
        this.id = ape[0] + ape2 + anio.getFullYear() + ran1 + ran2 + ran3 + ran4
    }

    mostrar(){
        frm.textContent = ""
        salida.classList.toggle("none")
        ti.textContent = this.id;
        tn.textContent = this.ape + ", " + this.nom;
        tcn.textContent = this.contra;
        tco.textContent = this.correo;
        tf.textContent = this.fechanac;
    }

}

function validarcorreo(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function validarfecha(fecha) {
    const re = /(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([1][26]|[2468][048]|[3579][26])00))))/;
    return re.test(String(fecha).toLowerCase());
}

ingresarbtn.addEventListener("click", () =>
{
    if (nomi.value && apei.value && contrai.value && contraci.value && correoi.value && fechanaci.value) {
        var exprCorreo = new RegExp("^", "i");
        if(validarcorreo(correoi.value)){
            if (contrai.value.length < 8){
                alert("la contraseña debe tener mas de 8 digítos");
            }else{
                if (contrai.value == contraci.value) {
                    if (validarfecha(fechanaci.value)) {
                        let nuevo = new miembro(nomi.value, apei.value, contraci.value, correoi.value, fechanaci.value);
                        nuevo.mostrar();
                    } else {
                        alert("La fecha fue ingresada mal")
                    }
                }else{
                    alert("las contrañas deben coincidir")
                }
            }
        }else{
            alert("Correo invalido!")
        }
    }else{
        alert("Debes ingresar todos los datos")
    }
})
