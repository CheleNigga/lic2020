// dom
const agregarColumna = document.getElementById("columnaExtra")
const agregarFila = document.getElementById("filaExtra")
const tabla = document.getElementById("contenidoTabla")
const filaI = document.getElementById("inputFila")
const columnaI = document.getElementById("inputColumna")
const textoI = document.getElementById("inputTexto")
const btn = document.getElementById("btn")
// variables
let columnas = 1
let filas = 1

agregarColumna.addEventListener("click",() => {
    columnas -=-1;
    for (let f = 0; f<filas;f-=-1){
        const fila = document.getElementById("f"+(f+1))
        const nuevaColumna = document.createElement("td")
        nuevaColumna.setAttribute("id", "f"+(f+1)+"c"+columnas)
        nuevaColumna.innerText = "fila "+(f+1)+", columna "+columnas
        fila.appendChild(nuevaColumna)
    }
})
agregarFila.addEventListener("click",()=>{
    filas -=-1
    const newfila = document.createElement("tr")
    newfila.setAttribute("id","f"+filas)
    tabla.appendChild(newfila)
    const fila = document.getElementById("f"+(filas))
    for (let c = 0; c<columnas;c-=-1){
        const nuevaColumna = document.createElement("td")
        nuevaColumna.setAttribute("id", "f"+(filas)+"c"+(c+1))
        nuevaColumna.innerText = "fila "+(filas)+", columna "+(c+1)
        fila.appendChild(nuevaColumna)
    }
})

btn.addEventListener("click", ()=>{
    if (filaI.value && columnaI.value && textoI.value){
        if (parseInt(filaI.value) && parseInt(columnaI.value)){
            let fi = parseInt(filaI.value)
            let ci = parseInt(columnaI.value)
            if ((fi > 0 && fi <= filas) && (ci > 0 && ci <= columnas)){
                document.getElementById("f"+fi+"c"+ci).innerText = textoI.value
            }else{
                alert("Unicamente puedes elegir entre las filas de 0 - "+filas+" y las columnas de 0 - "+columnas)
            }
        }else{
            alert("Debe ingresar números")
        }
    }else{
        alert("No puedes dejar los espacios vacios")
    }
})