const especial = document.getElementById("especial")
const regular = document.getElementById("regular")
const diesel = document.getElementById("diesel")
const galones = document.getElementById("galones")
const precio = document.getElementById("precio")

const esp = 4.27
const reg = 4.05
const die = 3.96
let tipo = "e"
especial.addEventListener("click",()=>{calculo("e");tipo = "e"})
regular.addEventListener("click",()=>{calculo("r");tipo = "r" })
diesel.addEventListener("click",()=>{calculo("d");tipo = "d"})
galones.addEventListener("change",()=>{calculo(tipo)})

const calculo =(tipo)=>{
    switch(tipo){
        case "e":
            precio.value = "$" + (esp * galones.value)
            break 
        case "r":
            precio.value = "$" + (reg * galones.value)
            break 
        case "d":
            precio.value = "$" + (die * galones.value)
            break 
    }
}