// dom
const numero = document.getElementById("numero");
const btn = document.getElementById("agregar");

// variables
let valores = [];
let colores = [];
let bordes = [];

numero.focus()

var ctx = document.getElementById("grafica").getContext("2d");
var grafica = new Chart(ctx, {
  type: "bar",
  data: {
    labels: ["Red"],
    datasets: [
      {
        label: "valores ingresados:",
        data: [12],
        backgroundColor: ["rgba(255, 99, 132, 0.2)"],
        borderColor: ["rgba(255, 99, 132, 1)"],
        borderWidth: 1,
      },
    ],
  },
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});

btn.addEventListener("click", () => {
  let color1 = Math.random() * 255;
  let color2 = Math.random() * 255;
  let color3 = Math.random() * 255;
  colores.push("rgba(" + color1 + ", " + color2 + ", " + color3 + ", 0.2)");
  bordes.push("rgba(" + color1 + ", " + color2 + ", " + color3 + ", 1)");
  valores.push(numero.value);
  grafica.data.labels = [...valores];
  grafica.data.datasets[0].data = [...valores];
  grafica.data.datasets[0].backgroundColor = [...colores];
  grafica.data.datasets[0].borderColor = [...bordes];
  grafica.update();
});

numero.addEventListener("keydown", (event) => {
  if (event.code === "Enter" || event.code === "NumpadEnter") {
    let color1 = Math.random() * 255;
    let color2 = Math.random() * 255;
    let color3 = Math.random() * 255;
    colores.push("rgba(" + color1 + ", " + color2 + ", " + color3 + ", 0.2)");
    bordes.push("rgba(" + color1 + ", " + color2 + ", " + color3 + ", 1)");
    valores.push(numero.value);
    grafica.data.labels = [...valores];
    grafica.data.datasets[0].data = [...valores];
    grafica.data.datasets[0].backgroundColor = [...colores];
    grafica.data.datasets[0].borderColor = [...bordes];
    grafica.update();
  }
});
