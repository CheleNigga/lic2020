// dom
const estilo = document.getElementById("estilo");
const limpiar = document.getElementById("limpiar");
const parrafo = document.getElementById("parrafo");

estilo.addEventListener("change", () => {
  switch (estilo.value) {
    case "estilo1":
      if (!parrafo.classList.contains("estilo1")) {
        parrafo.classList.toggle("estilo1");
      }
      if (parrafo.classList.contains("estilo2")) {
        parrafo.classList.toggle("estilo2");
      }
      if (parrafo.classList.contains("estilo3")) {
        parrafo.classList.toggle("estilo3");
      }
      break;
    case "estilo2":
      if (parrafo.classList.contains("estilo1")) {
        parrafo.classList.toggle("estilo1");
      }
      if (!parrafo.classList.contains("estilo2")) {
        parrafo.classList.toggle("estilo2");
      }
      if (parrafo.classList.contains("estilo3")) {
        parrafo.classList.toggle("estilo3");
      }
      break;
    case "estilo3":
      if (parrafo.classList.contains("estilo1")) {
        parrafo.classList.toggle("estilo1");
      }
      if (parrafo.classList.contains("estilo2")) {
        parrafo.classList.toggle("estilo2");
      }
      if (!parrafo.classList.contains("estilo3")) {
        parrafo.classList.toggle("estilo3");
      }
      break;
    case "blank":
      limpiarP();
      break;
  }
});
limpiar.addEventListener("click", () => {limpiarP();});
const limpiarP = () => {
  if (parrafo.classList.contains("estilo1")) {
    parrafo.classList.toggle("estilo1");
  }
  if (parrafo.classList.contains("estilo2")) {
    parrafo.classList.toggle("estilo2");
  }
  if (parrafo.classList.contains("estilo3")) {
    parrafo.classList.toggle("estilo3");
  }
  estilo.value = ""
};
