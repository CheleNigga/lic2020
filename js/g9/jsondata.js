function ajaxRequest() {
  //Crear array() con cadenas para creación de objeto ActiveX
  //en caso de navegadores antiguos de Internet Explorer
  var activexmodes = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];
  //Test for support for ActiveXObject in IE first (as XMLHttpRequest in IE7 is broken)
  if (window.ActiveXObject) {
    for (var i = 0; i < activexmodes.length; i++) {
      try {
        return new ActiveXObject(activexmodes[i]);
      } catch (e) {
        return false;
      }
    }
  }
  // Si se está usando Chrome, Mozilla, Safari, Opera, etc.
  else if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else {
    return false;
  }
}
function init() {
  var links = document.getElementsByClassName("link");
  var nav = document.getElementsByClassName("linkl");
  for (let i = 0; i < links.length; i++) {
    if (links[i].addEventListener) {
      links[i].addEventListener(
        "click",
        function (event) {
          if (event.preventDefault) {
            event.preventDefault();
          } else if (event.stopPropagation) {
            event.stopPropagation();
          } else {
            event.returnValue = false;
          }
          var request = new ajaxRequest();
          request.onreadystatechange = function () {
            if (request.readyState == 4) {
              if (
                request.status == 200 ||
                window.location.href.indexOf("http") == -1
              ) {
                //Recibir resultado como un objeto de JavaScript usando la función eval()
                //var jsondata = eval("("+request.responseText+")");
                //Recibir resultado como un objeto de JavaScript usando el método parse()
                var jsondata = JSON.parse(request.responseText);
                var rssentries = jsondata.peliculas;
                var output = `<h1>${rssentries[i].titulo}</h1>`;
                output += `<h3>Reparto:</h4>`;
                output += `<p>${rssentries[i].reparto}</p>`;
                output += `<h3>Sinopsis:</h4>`;
                output += `<p>${rssentries[i].sinopsis}</p>`;
                output += `<h3>Director/es:</h4>`;
                output += `<p>${rssentries[i].director}</p>`;
                document.getElementById("result").innerHTML = output;
              } else {
                alert("Ha ocurrido un error mientras se realizaba la petición");
              }
            }
          };
          request.open("GET", "../../js/g9/peliculas.json", true);
					request.send(null);
					for (let j = 0; j < nav.length; j-=-1){
						if (nav[j].classList.contains("active"))
							nav[j].classList.toggle("active")
					}
					nav[i].classList.toggle("active")
          return false;
        },
        false
      );
    } else if (links[i].attachEvent) {
      links[i].attachEvent("onclick", function (event) {
        if (event.preventDefault) {
          event.preventDefault();
        } else if (event.stopPropagation) {
          event.stopPropagation();
        } else {
          event.returnValue = false;
        }
        var request = new ajaxRequest();
        request.onreadystatechange = function () {
          if (request.readyState == 4) {
            if (
              request.status == 200 ||
              window.location.href.indexOf("http") == -1
            ) {
              //Recibir resultado como un objeto de JavaScript usando la función eval()
              //var jsondata = eval("("+request.responseText+")");
              //Recibir resultado como un objeto de JavaScript usando el método parse()
              var jsondata = JSON.parse(request.responseText);
              var rssentries = jsondata.peliculas;
              var output = `<h1>${rssentries[i].titulo}</h1>`;
              output += `<h3>Reparto:</h4>`;
              output += `<p>${rssentries[i].reparto}</p>`;
              output += `<h3>Sinopsis:</h4>`;
              output += `<p>${rssentries[i].sinopsis}</p>`;
              output += `<h3>Director/es:</h4>`;
              output += `<p>${rssentries[i].director}</p>`;
              document.getElementById("result").innerHTML = output;
            } else {
              alert("Ha ocurrido un error mientras se realizaba la petición");
            }
          }
        };
        request.open("GET", "../../js/g9/peliculas.json", true);
				request.send(null);
				for (let j = 0; j < nav.length; j-=-1){
					if (nav[j].classList.contains("active"))
						nav[j].classList.toggle("active")
				}
				nav[i].classList.toggle("active")
        return false;
      });
    }
  }
  var request = new ajaxRequest();
  request.onreadystatechange = function () {
    if (request.readyState == 4) {
      if (request.status == 200 || window.location.href.indexOf("http") == -1) {
        //Recibir resultado como un objeto de JavaScript usando la función eval()
        //var jsondata = eval("("+request.responseText+")");
        //Recibir resultado como un objeto de JavaScript usando el método parse()
        var jsondata = JSON.parse(request.responseText);
        var rssentries = jsondata.peliculas;
        var output = `<h1>${rssentries[0].titulo}</h1>`;
        output += `<h3>Reparto:</h4>`;
        output += `<p>${rssentries[0].reparto}</p>`;
        output += `<h3>Sinopsis:</h4>`;
        output += `<p>${rssentries[0].sinopsis}</p>`;
        output += `<h3>Director/es:</h4>`;
        output += `<p>${rssentries[0].director}</p>`;
        document.getElementById("result").innerHTML = output;
      } else {
        alert("Ha ocurrido un error mientras se realizaba la petición");
      }
    }
  };
  request.open("GET", "../../js/g9/peliculas.json", true);
  request.send(null);
}
if (window.addEventListener) {
  window.addEventListener("load", init, false);
} else if (window.attachEvent) {
  window.attachEvent("load", init);
}
