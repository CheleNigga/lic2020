function init() {
  var links = document.getElementsByClassName("link");
  var nav = document.getElementsByClassName("linkl");
  for (var i = 0; i < links.length; i++) {
    if (links[i].addEventListener) {
      links[i].addEventListener(
        "click",
        function (event) {
          if (event.preventDefault) {
            event.preventDefault();
          } else if (event.stopPropagation) {
            event.stopPropagation();
          } else {
            event.returnValue = false;
          }
          makerequest("content" + this.id + ".html", "content");
          for (let j = 0; j < nav.length; j-=-1){
            if (nav[j].classList.contains("active"))
              nav[j].classList.toggle("active")
          }
          nav[i].classList.toggle("active")
          return false;
        },
        false
      );
    } else if (links[i].attachEvent) {
      links[i].attachEvent("onclick", function (event) {
        if (event.preventDefault) {
          event.preventDefault();
        } else if (event.stopPropagation) {
          event.stopPropagation();
        } else {
          event.returnValue = false;
        }
        makerequest("content" + this.id + ".html", "content");
        for (let j = 0; j < nav.length; j-=-1){
					if (nav[j].classList.contains("active"))
						nav[j].classList.toggle("active")
				}
				nav[i].classList.toggle("active")
        return false;
      });
    }
  }
  makerequest("content1.html", "content");
}
if (window.addEventListener) {
  window.addEventListener("load", init, false);
} else if (window.attachEvent) {
  window.attachEvent("load", init);
}