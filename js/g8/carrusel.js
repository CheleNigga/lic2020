const imagen = document.getElementById("imagen")
const btnDerecha = document.getElementById("btnD")
const btnIzquierda = document.getElementById("btnI")

btnDerecha.addEventListener("click", () => {
    if (imagen.alt == 100){
        imagen.src = "../../src/img/25.jpg"
        imagen.alt = 25
    }else{
        imagen.alt = parseInt(imagen.alt, 10) + 25
        imagen.src = `../../src/img/${imagen.alt}.jpg`
    } 
})
btnIzquierda.addEventListener("click", () => {
    if (imagen.alt == 25){
        imagen.src = "../../src/img/100.jpg"
        imagen.alt = 100
    }else{
        imagen.alt = parseInt(imagen.alt, 10) - 25
        imagen.src = `../../src/img/${imagen.alt}.jpg`
    } 
})