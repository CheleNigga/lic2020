/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Materia: Desarrollo de Aplicaciones Web con *
* Software Interpretado en el Cliente *
* Archivo: circulo.js *
* Uso: Calcular el área de un círculo. *
* * * * * * * * * * * * * * * * * * * * * * * * * * * */

//funcion de validacion
function is_numeric(value) {
	return !isNaN(parseFloat(value));
}
const PI = 3.1415926535;
var radio = prompt('Introduzca el radio del círculo:','');
    if (is_numeric(radio)==false){
        do {
            radio = prompt('Error, no introduje bien el número, introduzcalo nuevamente:','');
        } while(is_numeric(radio) == false);
    }
var area;
area = PI*radio*radio;
document.write("<header><h1>El área del círculo es: " + area + "</h1><hr/><br /></header>");