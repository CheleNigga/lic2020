/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Materia: Desarrollo de Aplicaciones Web con *
* Software Interpretado en el Cliente *
* Archivo: triangulo.js *
* Uso: Calcular el área de un triángulo. *
* * * * * * * * * * * * * * * * * * * * * * * * * * * */
//funcion de validacion
function is_numeric(value) {
	return !isNaN(parseFloat(value));
}
var base = prompt('Introduzca la base del rectángulo','');
if (is_numeric(base)==false){
    do {
        base = prompt('Error, no introduje bien el número, introduzcalo nuevamente:','');
    } while(is_numeric(base) == false);
}
var altura = prompt('Introduzca la altura del rectángulo','');
if (is_numeric(altura)==false){
    do {
        altura = prompt('Error, no introduje bien el número, introduzcalo nuevamente:','');
    } while(is_numeric(altura) == false);
}
var area;
area = (base*altura)/2;
document.write("<header><h1>El área del Triángulo es: " + area + "</h1><hr/><br /></header>");
