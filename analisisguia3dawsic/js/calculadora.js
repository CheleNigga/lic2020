// Reconocimiento de la salida
const salida = document.querySelector('.operacion');

// reconocimiento de botones
const retroceder = document.querySelector('#retroceder');
const borrarTodo = document.querySelector('#bt');
const borrar = document.querySelector('#b');
const punto = document.querySelector('.punto')

// reconocimiento de numeros
const cero = document.querySelector('.cero');
const uno = document.querySelector('.uno');
const dos = document.querySelector('.dos');
const tres = document.querySelector('.tres');
const cuatro = document.querySelector('.cuatro');
const cinco = document.querySelector('.cinco');
const seis = document.querySelector('.seis');
const siete = document.querySelector('.siete');
const ocho = document.querySelector('.ocho');
const nueve = document.querySelector('.nueve');

// reconocimiento de operaciones
const suma = document.querySelector('.suma');
const resta = document.querySelector('.resta');
const multiplicacion = document.querySelector('.multiplicacion');
const division = document.querySelector('.division');
const residuo = document.querySelector('.residuo');
const raiz = document.querySelector('.sqrt');
const potencia = document.querySelector('.expo');
const inversa = document.querySelector('.inversa');
const memoryRetrieve = document.querySelector('.memoryRetrieve');
const memoryClear = document.querySelector('.memoryClear');
const memoryPlus = document.querySelector('.memoryPlus');
const memoryLess = document.querySelector('.memoryLess');
const igual = document.querySelector('.igual')

// Variables
var Memory = 0, ope = null, numero1, numero2;
var valor1 = new Array;
var valor2 = new Array;
valor1[0] = 0;
valor2[0] = 0;

// Funciones
var agregarNum = (num) => {
    if (ope == null){
        if (valor1[0] != 0){
            valor1.push(num);
        }else{
            valor1[0] = num;
        }
    }else{
        if(valor2[0] != 0){
            valor2.push(num);
        }else{
            valor2[0] = num;
        }
    }
}

var mostrar = () => {
    if (ope == null){
        numero1 = parseFloat(valor1.join(''));
        salida.textContent = numero1;
    }else{
        numero1 = parseFloat(valor1.join(''));
        numero2 = parseFloat(valor2.join(''));
        if (ope == "√"){
            if (numero1 == 0){
                salida.textContent = ope + numero2;
            }else{
                salida.textContent = numero1 + ope + numero2;
            }
        }else{
            salida.textContent = numero1 + ope + numero2;
        }
    }
}

var realizarOpe = () =>{
    if (ope != null){
        numero1 = parseFloat(valor1.join(''));
        numero2 = parseFloat(valor2.join(''));
        switch (ope){
            case '+':
                numero1 = numero1 + numero2;
                console.log(numero1);
                break;
            case '-':
                numero1 = numero1 - numero2;
                break;
            case '*':
                numero1 = numero1 * numero2;
                break;
            case '/':
                numero1 = numero1 / numero2;
                break;
            case '%':
                numero1 = numero1 % numero2;
                break;
            case '^':
                numero1 = Math.pow(numero1, numero2);
                break;
            case '√':
                if (numero1 == 0){
                    numero1 = Math.sqrt(numero2);
                }else{
                    numero1 = numero1 * Math.sqrt(numero2);
                }
                break;

        }
        var nuevonum = numero1.toString();
        ope = null;
        var total = valor1.length;
        for (var i = 0; i < total; i -= -1){
            valor1.pop();
        }
        valor1 = [...nuevonum];
        var total = valor2.length;
        for (var i = 0; i < total; i -= -1){
            valor2.pop();
        }
        valor2[0] = 0;
    }
}

var guardarMemoria = (signo) =>{
    if(ope != null & valor2[0] != 0){
        realizarOpe();
    }
    numero1 = parseFloat(valor1.join(''));
    switch (signo){
        case "+":
            Memory = numero1;
            break;
        case "-":
            Memory = -(numero1);
            break;
    }
}

// Funcionamiento
cero.addEventListener("click", () => { agregarNum(0); mostrar(); });
uno.addEventListener('click', () => { agregarNum(1); mostrar(); });
dos.addEventListener('click', () => { agregarNum(2); mostrar(); });
tres.addEventListener('click', () => { agregarNum(3); mostrar(); });
cuatro.addEventListener('click', () => { agregarNum(4); mostrar(); });
cinco.addEventListener('click', () => { agregarNum(5); mostrar(); });
seis.addEventListener('click', () => { agregarNum(6); mostrar(); });
siete.addEventListener('click', () => { agregarNum(7); mostrar(); });
ocho.addEventListener('click', () => { agregarNum(8); mostrar(); });
nueve.addEventListener('click', () => { agregarNum(9); mostrar(); });
suma.addEventListener('click', () => {
    if (ope == null){
        ope = "+";
    }else{
        realizarOpe();
        ope = "+"
    }
    mostrar();
});
resta.addEventListener('click', () => { 
    if (ope == null){
        ope = "-";
    }else{
        realizarOpe();
        ope = "-"
    }
    mostrar(); 
});
division.addEventListener('click', () => { 
    if (ope == null){
        ope = "/";
    }else{
        realizarOpe();
        ope = "/"
    } 
    mostrar(); 
});
multiplicacion.addEventListener('click', () => { 
    if (ope == null){
        ope = "*";
    }else{
        realizarOpe();
        ope = "*"
    } 
    mostrar(); 
});
potencia.addEventListener('click', () => { 
    if (ope == null){
        ope = "^";
    }else{
        realizarOpe();
        ope = "^"
    } 
    mostrar(); 
});
residuo.addEventListener('click', () => { 
    if (ope == null){
        ope = "%";
    }else{
        realizarOpe();
        ope = "%"
    }
    mostrar(); 
});
raiz.addEventListener('click', () => { 
    if (ope == null){
        ope = "√";
    }else{
        realizarOpe();
        ope = "√"
    } 
    mostrar(); 
});
igual.addEventListener('click', () => { realizarOpe(); mostrar();})
inversa.addEventListener('click', () => { 
    if (ope == null){
        numero1 = parseFloat(valor1.join(''));
        var apoyo =  numero1;
        numero1 = 1 / apoyo;
        var total = valor1.length;
        for (var i = 0; i < total; i -= -1){
            valor1.pop();
        }

        valor1 = [...numero1.toString()];
        mostrar();
    }else{
        realizarOpe();
        var apoyo =  numero1;
        numero1 = 1 / apoyo;
        var total = valor1.length;
        for (var i = 0; i < total; i -= -1){
            valor1.pop();
        }
        valor1 = [...numero1.toString()];
        mostrar();
    }
});
memoryPlus.addEventListener('click', () => { guardarMemoria("+"); });
memoryLess.addEventListener('click', () => { guardarMemoria("-"); });
memoryRetrieve.addEventListener('click', () => { 
    var total = valor1.length;
    for (var i = 0; i < total; i -= -1){
        valor1.pop();
    }
    valor1[0] =0;
    var total = valor2.length;
    for (var i = 0; i < total; i -= -1){
        valor2.pop();
    }
    valor2[0] = 0;
    ope=null;
    valor1[0] = Memory; 
    mostrar();
});
memoryClear.addEventListener('click', () => { Memory = 0 });
retroceder.addEventListener('click', () => { 
    if (ope == null){
        valor1.pop();
        if(valor1.length == 0){
            valor1[0]=0
        }
    }else{
        if (valor2[0]==0){
            ope=null;
        }else{
            valor2.pop();

            if(valor2.length == 0){
                valor2[0]=0
            }
        }
    }
    mostrar();
});
borrar.addEventListener('click', () => { 
    if (ope == null){
        var total = valor1.length;
        for (var i = 0; i < total; i -= -1){
            valor1.pop();
        }
        valor1[0] =0;
    }else{
        if (valor2[0] == 0){
            ope=null;
        }else{
            var total = valor2.length;
            for (var i = 0; i < total; i -= -1){
                valor2.pop();
            }
            valor2[0] = 0;
        }
    }
    mostrar();
});
borrarTodo.addEventListener('click', ()=>{
    var total = valor1.length;
    for (var i = 0; i < total; i -= -1){
        valor1.pop();
    }
    valor1[0] =0;
    var total = valor2.length;
    for (var i = 0; i < total; i -= -1){
        valor2.pop();
    }
    valor2[0] = 0;
    ope=null;
    mostrar();
});
punto.addEventListener('click', () => {
    if (ope==null){
        var total = valor1.length;
        var confirm = false;
        for(var i = 0; i < total ; i-= -1){
            if (valor1[i] == '.'){
                confirm = true;
            }
        }
        if (confirm == false){
            valor1.push('.');
        }
    }else{
        var total = valor2.length;
        var confirm = false;
        for(var i = 0; i < total ; i-= -1){
            if (valor2[i] == '.'){
                confirm = true;
            }
        }
        if (confirm == false){
            valor2.push('.');
        }
    }
    mostrar();
})